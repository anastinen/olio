/**
 *
 * @author Ana
 */

public class Theatre {
    private String ID;
    private String name;
    
    public Theatre(String i, String n) {
        ID = i;
        name = n;
    }
    
    public String getName() {
        return name;
    }

    public String getID() {
        return ID;
    }   
}
