
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author Ana
 */
public class TheaterList {
    private ArrayList<Theatre> theatres;
    private Document doc;
    
    public TheaterList() {
        parseCurrentData(getData("https://www.finnkino.fi/xml/TheatreAreas/"));
        theatres = theatresToList();
    }
    
    public String getData(String u) {
        String total = "";
        try {
            URL url;
            url = new URL(u);
      
            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
            String inputLine;
            while((inputLine = in.readLine()) != null) {
                total += inputLine + "\n";
            }
        in.close();
        } catch (MalformedURLException ex) {
            Logger.getLogger(TheaterList.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TheaterList.class.getName()).log(Level.SEVERE, null, ex);
        }
        return total;
    }
    
    private void parseCurrentData(String total) {
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(new InputSource(new StringReader(total)));
        } catch (ParserConfigurationException e) {
            System.err.println("Caught ParserConfigurationException!");
        } catch (IOException e) {
            System.err.println("Caught IOException!");
        } catch (SAXException e) {
            System.err.println("Caught SAXException!");
        }
    }
    
    private ArrayList theatresToList() {
        String nam,id;
        ArrayList<Theatre> ts = new ArrayList();
        
        NodeList nodes = doc.getElementsByTagName("TheatreArea");
        for(int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element e = (Element) node;
            nam = getValue("Name", e);
            id = getValue("ID", e);
            Theatre t = new Theatre(id, nam);
            ts.add(t);
        }   
        return ts;
    }
    
    private String getValue(String tag, Element e) {
        return ((Element) (e.getElementsByTagName(tag).item(0))).getTextContent();
    }
    
    public ArrayList getNameList() {
        ArrayList<String> names = new ArrayList();
        
        for(Theatre t : theatres) {
            names.add(t.getName());
        }
        return names;  
    }
    
    public ArrayList showMovies(String n, String date, String start, String end) throws NullPointerException {
        // Finds the shows in the given theatre in the given date in the given interval
        String nam, startTime;
        ArrayList<String> names = new ArrayList();
        LocalDateTime startDateTime, endDateTime;
        
        // Check the given starting and ending times
        // In case of no input, set the starting time to 00:00 and ending time to 23:59
        if (start.isEmpty()) {
            start = "00:00";
        }
        if (end.isEmpty()) {
            end = "23:59";
        }
        // Is no date is given, current date is used to search for movies
        if (date.isEmpty()) {
            Date currentDate = new Date();
            SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");  
            date = formatter.format(currentDate);  
        }
        // Split the date into components
        String[] dateSplit = date.split("\\.");
        String first = dateSplit[2];
        String second = dateSplit[1];
        String third = dateSplit[0];
        
        // Format the date correctly and create datetime objects for movie start and end        
        String dateFormat = first + "-" + second + "-" + third; 
        startDateTime = LocalDateTime.parse(dateFormat + "T" + start + ":00");
        endDateTime = LocalDateTime.parse(dateFormat + "T" + end + ":00");
        
        // Find the chosen theatre, get the shows within the chosen interval
        Theatre t = findTheatre(n);
        DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");
        parseCurrentData(getData("http://www.finnkino.fi/xml/Schedule/?area=" + t.getID() + "&dt=" + date));
        NodeList nodes = doc.getElementsByTagName("Show");
        for(int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element e = (Element) node;
            nam = getValue("Title", e);
            startTime = getValue("dttmShowStart", e);
            LocalDateTime d = LocalDateTime.parse(startTime);
            if (d.isBefore(startDateTime) || d.isAfter(endDateTime)) { 
            }
            else {
                names.add(nam + " " + formatter2.format(d));
            }
        }
        return names;
    }
    
    public Theatre findTheatre(String n) {
        for (Theatre t : theatres) {
            if (t.getName().equals(n)) {
                return t;
            } 
        } 
        return null;
    }
    
    public ArrayList findTheatres(String n, String date, String start, String end) throws NullPointerException {
        // Finds the shows in the given theatre in the given date in the given interval
        String nam, startTime;
        ArrayList<String> names = new ArrayList();
        LocalDateTime startDateTime, endDateTime;
        
        names.add(n);
        // Check the given starting and ending times
        // In case of no input, set the starting time to 00:00 and ending time to 23:59
        if (start.isEmpty()) {
            start = "00:00";
        }
        if (end.isEmpty()) {
            end = "23:59";
        }
        // Is no date is given, current date is used to search for movies
        if (date.isEmpty()) {
            Date currentDate = new Date();
            SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");  
            date = formatter.format(currentDate);  
        }
        // Split the date into components
        String[] dateSplit = date.split("\\.");
        String first = dateSplit[2];
        String second = dateSplit[1];
        String third = dateSplit[0];
        
        // Format the date correctly and create datetime objects for movie start and end        
        String dateFormat = first + "-" + second + "-" + third; 
        startDateTime = LocalDateTime.parse(dateFormat + "T" + start + ":00");
        endDateTime = LocalDateTime.parse(dateFormat + "T" + end + ":00");
        
        // Find the chosen theatre, get the shows within the chosen interval
        // Pretty sure the nationwide schedule only shows current day
        // Can't be bothered to loop through all theatres
        DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm"); 
        parseCurrentData(getData("http://www.finnkino.fi/xml/Schedule/"));
        NodeList nodes = doc.getElementsByTagName("Show");
        for(int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element e = (Element) node;
            if (getValue("Title", e).contains(n)) {
                nam = getValue("Theatre", e);
                startTime = getValue("dttmShowStart", e);
                LocalDateTime d = LocalDateTime.parse(startTime);
                if (d.isBefore(startDateTime) || d.isAfter(endDateTime)) { 
                }
                else {
                    names.add(nam + " " + formatter2.format(d));
                }
            }
        }
        return names;
    }
}
