/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

/**
 *
 * @author Ana
 */
public class FXMLDocumentController implements Initializable {
    
    private Label label;
    @FXML
    private Button button;
    @FXML
    private Label headLabel;
    @FXML
    private Label chooseTheaterLabel;
    @FXML
    private Label timeSpanLabel;
    @FXML
    private Label movieNameLabel;
    @FXML
    private TextField movieNameField;
    @FXML
    private Button nameSearchButton;
    @FXML
    private TextField startTimeField;
    @FXML
    private TextField endTimeField;
    @FXML
    private TextField dateField;
    @FXML
    private ComboBox<String> theaterMenu;
    @FXML
    private ListView<String> movieDisplay;
    private TheaterList theaters;
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        theaters = new TheaterList();
        ObservableList<String> obsThe = FXCollections.observableArrayList(theaters.getNameList());
        theaterMenu.setItems(obsThe);
    }    

    @FXML
    private void listMovies(ActionEvent event) {
        try {
            movieDisplay.setItems(FXCollections.observableArrayList(
                    theaters.showMovies(theaterMenu.getValue(), 
                            dateField.getText(), 
                            startTimeField.getText(), 
                            endTimeField.getText())));
        } catch (NullPointerException e) {
            //System.out.println("Anna teatteri!");
        }  
    }

    @FXML
    private void searchNames(ActionEvent event) {
        try {
            movieDisplay.setItems(FXCollections.observableArrayList(
                    theaters.findTheatres(movieNameField.getText(), 
                            dateField.getText(), 
                            startTimeField.getText(), 
                            endTimeField.getText())));
        } catch (NullPointerException e) {
            //System.out.println("Anna teatteri!");
        } 
    } 
}
