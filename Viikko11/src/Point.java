
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

/**
 * @author Antti Vilkman 0521281
 * 11.06.2018
 * Olio-ohjelmoinnin perusteet, viikko 11
 */

public class Point {
    private String name = "piste";
    private Circle c;
    private int index;
    private boolean isBound = false;
    
    
    public Point(double x, double y, int i) {
        c = new Circle();
        c.setCenterX(x);
        c.setCenterY(y);
        c.setRadius(10);
        c.setFill(Color.BLUE);
        c.setOnMouseClicked((MouseEvent event) -> {
            System.out.println("Hei, olen " + name + "!");
        });
    }
    
    public Circle getCircle() {
        return c;
    }
    
    public int getIndex() {
        return index;
    }
    
    public void bind() {
        isBound = true;
    }
    
    public void unbind() {
        isBound = false;
    }
    
    public boolean isBound() {
        return isBound;
    } 
}
