/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;

/**
 * @author Antti Vilkman 0521281
 * 11.06.2018
 * Olio-ohjelmoinnin perusteet, viikko 11
 */

public class FXMLDocumentController implements Initializable {
    
    @FXML
    private AnchorPane mainFrame;
    private ShapeHandler shapes;
    private Line l;
    @FXML
    private Pane mainPane;
   
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        l = new Line();
        shapes = ShapeHandler.create();
        mainPane.setOnMouseClicked((MouseEvent event) -> {
            Object o = event.getSource();
            if((event.getSource() instanceof Circle)) {
            }
            else {
                createDot(event.getX(), event.getY());
            }
        });
    }    


    private void createDot(double x, double y) {
        Point p = new Point(x, y, mainFrame.getChildren().size()); 
        shapes.addPoint(p);
        mainFrame.getChildren().add(p.getCircle());
        p.getCircle().setOnMousePressed((MouseEvent event) -> {
            createLine(p.getCircle());
        });
    }  
    
    private void createLine(Circle c) {
        if (!l.startXProperty().isBound() || (l.startXProperty().isBound() && l.endXProperty().isBound())) {
            l = new Line();
            l.startXProperty().bind(c.centerXProperty());
            l.startYProperty().bind(c.centerYProperty());
        }
        else {
            l.endXProperty().bind(c.centerXProperty());
            l.endYProperty().bind(c.centerYProperty());
            mainFrame.getChildren().add(l);
            shapes.addLine(l);
        }
    }
}
