
import java.util.ArrayList;
import javafx.scene.shape.Line;

/**
 * @author Antti Vilkman 0521281
 * 11.06.2018
 * Olio-ohjelmoinnin perusteet, viikko 11
 */

public class ShapeHandler {
    private ArrayList<Point> pointList;
    private ArrayList<Line> lineList;
    private static ShapeHandler unique;
    
    private  ShapeHandler() {
        pointList = new ArrayList();
        lineList = new ArrayList();
    }
    
    public static ShapeHandler create() {
        if (unique == null) {
            unique = new ShapeHandler();
        }
        else {}
        return unique;
    }
    
    public void addPoint(Point p) {
        pointList.add(p);
    }
    
    public ArrayList getList() {
        return pointList;
    }
    
    public void addLine(Line l) {
        lineList.add(l);
    }
}
