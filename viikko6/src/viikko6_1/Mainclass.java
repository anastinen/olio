package viikko6_1;

import java.util.Scanner;

/**
 * Viikko6 tehtävä 1
 * @author Antti Vilkman 0521281
 * Olio-Ohjelmoinnin perusteet
 * 30.5.2018
 * Mainclass.java
 */

public class Mainclass {

    public static void main(String[] args) {
        OUTER:
            while(true) {
                System.out.println("\n*** Pankkijärjestelmä ***");
                System.out.println("1) Lisää tavallinen tili");
                System.out.println("2) Lisää luotollinen tili");
                System.out.println("3) Tallenna tilille rahaa");
                System.out.println("4) Nosta tililtä");
                System.out.println("5) Poista tili");
                System.out.println("6) Tulosta tili");
                System.out.println("7) Tulosta kaikki tilit");
                System.out.println("0) Lopeta");
                System.out.print("Valintasi: ");
                Scanner s = new Scanner(System.in);
                String input = s.next();
                try {
                    switch (Integer.parseInt(input)) {
                        case 0:
                            s.close();
                            break OUTER;
                        case 1:
                            addNormalAccount();
                            break;
                        case 2:
                            addCreditAccount();
                            break;
                        case 3:
                            deposit();
                            break;
                        case 4:
                            withdraw();
                            break;
                        case 5:
                            deleteAccount();
                            break;
                        case 6:
                            printAccount();
                            break;
                        case 7:
                            printAllAccounts();
                            break;
                        default:
                            System.out.println("Valinta ei kelpaa.");
                            break;
                    }
                } catch (NumberFormatException e) {
                    System.out.println("Valinta ei kelpaa.");
                }
        }
    }
    public static void addNormalAccount() {
        Scanner s = new Scanner(System.in);
        System.out.print("Syötä tilinumero: ");
        s.next();
        System.out.print("Syötä rahamäärä: ");
        s.next();
        System.out.println("Tilinumero: ");
        System.out.println("Rahamäärä: ");
    }
    
    public static void addCreditAccount() {
        Scanner s = new Scanner(System.in);
        System.out.print("Syötä tilinumero: ");
        s.next();
        System.out.print("Syötä rahamäärä: ");
        s.next();
        System.out.print("Syötä luottoraja: ");
        s.next();
        System.out.println("Tilinumero: ");
        System.out.println("Rahamäärä: ");
        System.out.println("Luotto: ");
    }
    
    public static void deposit() {
        Scanner s = new Scanner(System.in);
        System.out.print("Syötä tilinumero: ");
        s.next();
        System.out.print("Syötä rahamäärä: ");
        s.next();
        System.out.println("Tilinumero: ");
        System.out.println("Rahamäärä: ");
    }
    
    public static void withdraw() {
        Scanner s = new Scanner(System.in);
        System.out.print("Syötä tilinumero: ");
        s.next();
        System.out.print("Syötä rahamäärä: ");
        s.next();
        System.out.println("Tilinumero: ");
        System.out.println("Rahamäärä: ");
    }
    
    public static void deleteAccount() {
        Scanner s = new Scanner(System.in);
        System.out.println("Syötä poistettava tilinumero: ");
        s.next();
        System.out.println("Tilinumero: ");
    }
    
    public static void printAccount() {
        Scanner s = new Scanner(System.in);
        System.out.print("Syötä tulostettava tilinumero: ");
        s.next();
        System.out.println("Tilinumero: ");
    }
    
    public static void printAllAccounts() {
        System.out.println("Tulostaa kaiken");

    }
}
