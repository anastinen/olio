package viikko6_4;

import java.util.HashMap;
import java.util.Scanner;

/**
 * Viikko6 tehtävä 4
 * @author Antti Vilkman 0521281
 * Olio-Ohjelmoinnin perusteet
 * 30.5.2018
 * Bank.java
 */

public class Bank {
    private HashMap<String, Account> accounts;
    
    public Bank() {  
        accounts = new HashMap<>();
    }
    
    public void addAccount() {
        String num;
        int amount;
        Scanner s = new Scanner(System.in);
        System.out.print("Syötä tilinumero: ");
        num = s.next();
        System.out.print("Syötä rahamäärä: ");
        amount = Integer.parseInt(s.next());
        accounts.put(num, new normalAccount(num, amount));
        System.out.println("Tili luotu.");
    }
    
    public void addCreditAccount() {
        String num;
        int amount, credit;
        Scanner s = new Scanner(System.in);
        System.out.print("Syötä tilinumero: ");
        num = s.next();
        System.out.print("Syötä rahamäärä: ");
        amount = Integer.parseInt(s.next());
        System.out.print("Syötä luottoraja: ");
        credit = Integer.parseInt(s.next());
        System.out.println("Pankkiin lisätään: " + num + "," +  amount + "," + credit);
    }
    
    public void deposit() {
        String num;
        int amount;
        Scanner s = new Scanner(System.in);
        System.out.print("Syötä tilinumero: ");
        num = s.next();
        System.out.print("Syötä rahamäärä: ");
        amount = Integer.parseInt(s.next());
        System.out.println("Talletetaan tilille: " + num + " rahaa " + amount);
    }
    
    public void withdraw() {
        String num;
        int amount;
        Scanner s = new Scanner(System.in);
        System.out.print("Syötä tilinumero: ");
        num = s.next();
        System.out.print("Syötä rahamäärä: ");
        amount = Integer.parseInt(s.next());
        accounts.get(num).withdraw(amount);
    }
        
    public void deleteAccount() {
        String num;
        Scanner s = new Scanner(System.in);
        System.out.print("Syötä poistettava tilinumero: ");
        num = s.next();
        accounts.remove(num);
        System.out.println("Tili poistettu.");
        
    }
    
    public void printAccount() {
        String num;
        Scanner s = new Scanner(System.in);
        System.out.print("Syötä tulostettava tilinumero: ");
        num = s.next();
        accounts.get(num).print();
    }
    
    public void printAllAccounts() {
        System.out.println("Kaikki tilit:");
    }
        
}

