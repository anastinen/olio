package viikko6_4;

/**
 * Viikko6 tehtävä 4
 * @author Antti Vilkman 0521281
 * Olio-Ohjelmoinnin perusteet
 * 30.5.2018
 * Account.java
 */

abstract class Account {
    protected String num;
    protected int money;
    
    abstract public void print(); 
    
    protected void deposit(int amount) {
        money += amount;
    }
    
    protected void withdraw(int amount) {
        if(money - amount >= 0) {
            money -= amount;
        } 
        else {
            System.out.println("Ei tarpeeksi rahaa tilillä.");
        }
    }
}

class normalAccount extends Account {
    
    public normalAccount(String n, int m) {
        num = n;
        money = m;
    }
    
    @Override
    public void print() {
        System.out.println("Tilinumero: " + num + " Tilillä rahaa: " + money);
    }
}