package viikko6_3;

import java.io.IOException;
import java.util.Scanner;

/**
 * Viikko6 tehtävä 3
 * @author Antti Vilkman 0521281
 * Olio-Ohjelmoinnin perusteet
 * 30.5.2018
 * Mainclass.java
 */

public class Mainclass {

    public static void main(String[] args) throws IOException {
        Bank b = new Bank();
        while(true) {
            System.out.println("\n*** PANKKIJÄRJESTELMÄ ***");
            System.out.println("1) Lisää tavallinen tili");
            System.out.println("2) Lisää luotollinen tili");
            System.out.println("3) Tallenna tilille rahaa");
            System.out.println("4) Nosta tililtä");
            System.out.println("5) Poista tili");
            System.out.println("6) Tulosta tili");
            System.out.println("7) Tulosta kaikki tilit");
            System.out.println("0) Lopeta");
            System.out.print("Valintasi: ");
            Scanner s = new Scanner(System.in);
            String input = s.next();

            switch(Integer.parseInt(input)) {
                case 0:
                    s.close();
                    return;
                case 1:
                    b.addAccount();
                    break;
                case 2:
                    b.addCreditAccount();
                    break;
                case 3:
                    b.deposit();
                    break;
                case 4:
                    b.withdraw();
                    break;
                case 5:
                    b.deleteAccount();
                    break;
                case 6:
                    b.printAccount();
                    break;
                case 7:
                    b.printAllAccounts();
                    break;
                default:
                    System.out.println("Valinta ei kelpaa.");
                    break;
            }
        }
    }
}
