package viikko6_3;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Viikko6 tehtävä 3
 * @author Antti Vilkman 0521281
 * Olio-Ohjelmoinnin perusteet
 * 30.5.2018
 * Bank.java
 */

public class Bank {
    private ArrayList accounts;
    
    public Bank() {    
    }
    
    public void addAccount() {
        String num;
        int amount;
        Scanner s = new Scanner(System.in);
        System.out.print("Syötä tilinumero: ");
        num = s.next();
        System.out.print("Syötä rahamäärä: ");
        amount = Integer.parseInt(s.next());
        System.out.println("Pankkiin lisätään: " + num + "," +  amount);
    }
    
    public void addCreditAccount() {
        String num;
        int amount, credit;
        Scanner s = new Scanner(System.in);
        System.out.print("Syötä tilinumero: ");
        num = s.next();
        System.out.print("Syötä rahamäärä: ");
        amount = Integer.parseInt(s.next());
        System.out.print("Syötä luottoraja: ");
        credit = Integer.parseInt(s.next());
        System.out.println("Pankkiin lisätään: " + num + "," +  amount + "," + credit);
    }
    
    public void deposit() {
        String num;
        int amount;
        Scanner s = new Scanner(System.in);
        System.out.print("Syötä tilinumero: ");
        num = s.next();
        System.out.print("Syötä rahamäärä: ");
        amount = Integer.parseInt(s.next());
        System.out.println("Talletetaan tilille: " + num + " rahaa " + amount);
    }
    
    public void withdraw() {
        String num;
        int amount;
        Scanner s = new Scanner(System.in);
        System.out.print("Syötä tilinumero: ");
        num = s.next();
        System.out.print("Syötä rahamäärä: ");
        amount = Integer.parseInt(s.next());
        System.out.println("Nostetaan tililtä: " + num + " rahaa " + amount);
    }
        
    public void deleteAccount() {
        String num;
        Scanner s = new Scanner(System.in);
        System.out.print("Syötä poistettava tilinumero: ");
        num = s.next();
        System.out.println("Tili poistettu.");
    }
    
    public void printAccount() {
        String num;
        Scanner s = new Scanner(System.in);
        System.out.print("Syötä tulostettava tilinumero: ");
        num = s.next();
        System.out.println("Etsitään tiliä: " + num);
    }
    
    public void printAllAccounts() {
        System.out.println("Kaikki tilit:");
    }
        
}
