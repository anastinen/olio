package viikko6_5;

/**
 * Viikko6 tehtävä 5
 * @author Antti Vilkman 0521281
 * Olio-Ohjelmoinnin perusteet
 * 30.5.2018
 * Account.java
 */

abstract class Account {
    protected String num;
    protected int money;
    
    abstract public void print(); 
    
    protected void putMoney(int amount) {
        money += amount;
    }
    
    protected void takeMoney(int amount) {
        if(money - amount >= 0) {
            money -= amount;
        } 
        else {
            System.out.println("Ei tarpeeksi rahaa tilillä.");
        }
    }
}

class normalAccount extends Account {
    
    public normalAccount(String n, int m) {
        num = n;
        money = m;
    }
    
    @Override
    public void print() {
        System.out.println("Tilinumero: " + num + " Tilillä rahaa: " + money);
    }
}

class creditAccount extends Account {
    private int credit;
    
    public creditAccount(String n, int m, int c) {
        num = n;
        money = m;
        credit = c;
    }
    
    @Override
    public void takeMoney(int amount) {
        if(money + credit - amount >= 0) {
            money -= amount;
        } 
        else {
            System.out.println("Ei tarpeeksi rahaa tilillä.");
        }
    }
    
    @Override
    public void print() {
        System.out.println("Tilinumero: " + num + " Tilillä rahaa: " + money + " Luottoraja: " + credit);
    }
}