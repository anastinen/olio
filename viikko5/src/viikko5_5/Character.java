package viikko5_5;

/**
 * Viikko5 tehtävä 5
 * @author Antti Vilkman 0521281
 * Olio-Ohjelmoinnin perusteet
 * 29.5.2018
 * Character.java
 */

class Character {
    public WeaponBehaviour weapon;
    protected String name;
    
    public Character(){
    }
    
    public void fight() {
        System.out.println(name + " tappelee aseella " + weapon.getName());
        weapon.useWeapon();
    }
}

class King extends Character {
    public King() {
    name = "King";
    }
}

class Knight extends Character {
    public Knight() {
    name = "Knight";
    }
}

class Queen extends Character {
    public Queen() {
    name = "Queen";
    }
}

class Troll extends Character {
    public Troll() {
    name = "Troll";
    }
}

abstract class WeaponBehaviour {
    protected String name;
    private Character owner;
    
    public void useWeapon() {
    }
    
    public String getName() {
        return name;
    }
}

class KnifeBehaviour extends WeaponBehaviour {
    public KnifeBehaviour() {
        name = "Knife";
    }
}

class AxeBehaviour extends WeaponBehaviour {
    public AxeBehaviour() {
        name = "Axe";
    }
}

class SwordBehaviour extends WeaponBehaviour {
    public SwordBehaviour() {
        name = "Sword";
    }
}

class MaceBehaviour extends WeaponBehaviour {
    public MaceBehaviour() {
        name = "Mace";
    }
}
