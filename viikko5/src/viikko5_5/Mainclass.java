package viikko5_5;

import java.util.Scanner;

/**
 * Viikko5 tehtävä 5
 * @author Antti Vilkman 0521281
 * Olio-Ohjelmoinnin perusteet
 * 29.5.2018
 * Mainclass.java
 */

public class Mainclass {
    private static Character myChar;

    public static void main(String[] args) {       
        OUTER:
        while(true) {
            System.out.println("*** TAISTELUSIMULAATTORI ***");
            System.out.println("1) Luo hahmo");
            System.out.println("2) Taistele hahmolla");
            System.out.println("0) Lopeta");
            System.out.print("Valintasi: ");
            Scanner s = new Scanner(System.in);
            String input = s.next();
            try {
                switch (Integer.parseInt(input)) {
                    case 0:
                        s.close();
                        break OUTER;
                    case 1:
                        myChar = charMenu();
                        myChar.weapon = weaponMenu();
                        break;
                    case 2:
                        myChar.fight();
                        break;
                    default:
                        System.out.println("Väärä valinta, peikko pyytää uutta yritystä.");
                        break;
                }
            } catch (NumberFormatException e) {
                System.out.println("Väärä valinta, peikko pyytää uutta yritystä.");
            }
        }
    }
    
    public static Character charMenu() {
        OUTER:
        while(true) {
            System.out.println("Valitse hahmosi: ");
            System.out.println("1) Kuningas");
            System.out.println("2) Ritari");
            System.out.println("3) Kuningatar");
            System.out.println("4) Peikko");
            System.out.print("Valintasi: ");
            Scanner s = new Scanner(System.in);
            String input = s.next();
            try {
                switch (Integer.parseInt(input)) {
                    case 1:
                        return new King();
                    case 2:
                        return new Knight();
                    case 3:
                        return new Queen();
                    case 4:
                        return new Troll();
                    default:
                        System.out.println("Väärä valinta, peikko pyytää uutta yritystä.");
                }
            } catch (NumberFormatException e) {
                System.out.println("Väärä valinta, peikko pyytää uutta yritystä.");
            }
        }
    }
    
    public static WeaponBehaviour weaponMenu() {
        OUTER:
        while(true) {
            System.out.println("Valitse aseesi: ");
            System.out.println("1) Veitsi");
            System.out.println("2) Kirves");
            System.out.println("3) Miekka");
            System.out.println("4) Nuija");
            System.out.print("Valintasi: ");
            Scanner s = new Scanner(System.in);
            String input = s.next();
            try {
                switch (Integer.parseInt(input)) {
                    case 1:
                        return new KnifeBehaviour();
                    case 2:
                        return new AxeBehaviour();
                    case 3:
                        return new SwordBehaviour();
                    case 4:
                        return new MaceBehaviour();
                    default:
                        System.out.println("Väärä valinta, peikko pyytää uutta yritystä.");
                        break;
                }
            } catch (NumberFormatException e) {
                System.out.println("Väärä valinta, peikko pyytää uutta yritystä.");
            }
        }
    }
}

    

