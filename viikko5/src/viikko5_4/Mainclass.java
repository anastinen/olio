package viikko5_4;

/**
 * Olio-Ohjelmoinnin Perusteet
 * @author Antti Vilkman 0521281
 * 30.05.2018
 * Viikko 5, tehtävä 4
 * Mainclass.java
 */

public class Mainclass {

    public static void main(String[] args) {
        Car care = new Car();
        care.print();
    }
    
}
