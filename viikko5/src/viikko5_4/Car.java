package viikko5_4;

import java.util.ArrayList;

/**
 * Olio-Ohjelmoinnin Perusteet
 * @author Antti Vilkman 0521281
 * 30.05.2018
 * Viikko 5, tehtävä 4
 * Car.java
 */

public class Car {
    private Body body;
    private Engine engine;
    private Chassis chassis;
    private Wheel wheel1;
    private Wheel wheel2;
    private Wheel wheel3;
    private Wheel wheel4;
    private ArrayList<Part> parts = new ArrayList();
    
    public Car() {
        body = new Body();
        parts.add(body);
        chassis = new Chassis();
        parts.add(chassis);
        engine = new Engine();
        parts.add(engine); 
        wheel1 = new Wheel();
        parts.add(wheel1);
        wheel2 = new Wheel();
        parts.add(wheel2);
        wheel3 = new Wheel();
        parts.add(wheel3);
        wheel4 = new Wheel();
        parts.add(wheel4);
    }
    
    public void print() {
        System.out.println("Autoon kuuluu:");
        parts.forEach((p) -> {
            int count = 0;
            for(Part o : parts) {
                if(p.name.equals(o.name)) {
                    count += 1;
                }
                if(o.multiples) {
                    p.multiples = true;
                    break;
                }
            }
            if(p.multiples) {
                
            }
            else if(count<2) {
                System.out.println("	" + p.getName());
            }
            else { 
                System.out.println("	" + count + " " + p.getName());
                p.multiples = true;
            }
        });    
    }
}

abstract class Part {
    protected boolean multiples;
    protected boolean isBroken = false; 
    protected int price;
    protected int weight;
    protected String name;
    
    public String getName() {
        return name;
    }
}

class Body extends Part {
    
    public Body() {
        name = "Body";
        System.out.println("Valmistetaan: Body");
    }
}

class Chassis extends Part {
    
    public Chassis() {
        name = "Chassis";
        System.out.println("Valmistetaan: Chassis");
    }
}

class Wheel extends Part {
    // Tells how worn the tire is, int from 100 to 0, with 0 being broken
    private int weariness = 100;
    //Implement functionality for breaking later
    
    public Wheel() {
        name = "Wheel";
        System.out.println("Valmistetaan: Wheel");  
    }
}

class Engine extends Part {
    private int temperature;
    private int oilLeft;
    private boolean isOn;
    
    public Engine() {
        name = "Engine";
        System.out.println("Valmistetaan: Engine");
    }
    
    public void turnOn() {
        isOn = true;
    }
        
    public void turnOff() {
        isOn = false;
    }
}





