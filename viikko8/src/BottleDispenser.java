

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Viikko8 tehtävä kaikki
 * @author Antti Vilkman 0521281
 * Olio-Ohjelmoinnin perusteet
 * 29.5.2018
 * BottleDispenser.java
 */

public class BottleDispenser {
    private int bottles;
    private double money;
    private String receipt;
    // The array for the Bottle-objects
    private ArrayList<Bottle> bottle_array;
    private static BottleDispenser unique;
    
    private BottleDispenser() {
        bottles = 5;
        money = 0;
        // Make the bottle-array
        bottle_array = new ArrayList();
        // Add Bottles to the array
        bottle_array.add(new Bottle());
        bottle_array.add(new Bottle("Pepsi", "Pepsi Max", 1.5, 2.2));
        bottle_array.add(new Bottle("Coca-Cola", "Coca-Cola Zero", 0.5, 2.0));
        bottle_array.add(new Bottle("Coca-Cola", "Coca-Cola Zero", 1.5, 2.5));
        bottle_array.add(new Bottle("Coca-Cola", "Fanta Zero", 0.5, 1.95));
        bottle_array.add(new Bottle("Coca-Cola", "Fanta Zero", 0.5, 1.95)); 
    }
    
    public static BottleDispenser create() {
        if (unique == null) {
            unique = new BottleDispenser();
        }
        return unique;
    }
    
    public String addMoney(Double amount) {
        money += amount;
        return("Klink! Lisää rahaa laitteeseen!");
    }
    
    public String buyBottle(String name, Double size) {
        String result;
        Bottle chosen = null;
        
        for (Bottle b: bottle_array) {
            if (b.getName().equals(name) && b.getSize() == size) {
                chosen = b;
            }
        }
        if (chosen == null) {
            return name + String.format(" %.2f", size) + "l loppu!";
        }
        if(money<chosen.getPrice()) {
            result = "Syötä rahaa ensin!";
        } 
        else {      
            result = "KACHUNK! " + chosen.getName() + " " + chosen.getSize() + "l tipahti masiinasta!";
            money -= chosen.getPrice();
            receipt = "Ostos: " + chosen.getName() + " " + chosen.getSize() + "l " + chosen.getPrice() + "€";
            bottle_array.remove(chosen);
        }
        return result;
    }
    
    public String returnMoney() {
        String result;
        result = ("Klink klink. Sinne menivät rahat!\nRahaa tuli ulos " 
                + String.format("%.2f", money).replace(".", ",") + "€");
        money = 0;
        return result;
    }
    
    public void removeBottle(int index) {
        // Removes the last bottle in the array
        bottle_array.remove(index);
        bottles -= 1;
    }
    
    public ArrayList<String> getBottleNames() {
        ArrayList<String> bottleNames = new ArrayList();
        for (Bottle b : bottle_array) {
            if (bottleNames.contains(b.getName())) {
            }
            else {
            bottleNames.add(b.getName());
            }
        }
        return bottleNames;
    }
    
    public String saveReceipt() throws IOException {
        BufferedWriter out;
        
        out = new BufferedWriter(new FileWriter("kuitti.txt"));
        if (receipt == null) {
            return "Ei aiempia ostoksia.";
        }
        out.write(receipt);
        out.close();
        return "Kuitti tallennettu.";
    }
}