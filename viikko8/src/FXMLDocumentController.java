/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextArea;
import javafx.scene.input.DragEvent;
import javafx.scene.input.MouseEvent;

/**
 * Viikko8 tehtävä kaikki
 * @author Antti Vilkman 0521281
 * Olio-Ohjelmoinnin perusteet
 * 29.5.2018
 * FXMLDocumentController.java
 */

public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Label label;
    @FXML
    private Button moneyHole;
    @FXML
    private Button buyButton;
    @FXML
    private Button moneyBackButton;
    @FXML
    private TextArea textDisplay;
    private BottleDispenser d;
    private ComboBox<String> bottleMenu;
    @FXML
    private Slider moneySlider;
    @FXML
    private Label sliderAmount;
    @FXML
    private ComboBox<String> brandMenu;
    @FXML
    private ComboBox<Double> sizeMenu;
    @FXML
    private Button receipt;
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        d = BottleDispenser.create();
        brandMenu.setItems(FXCollections.observableList(d.getBottleNames()));
        sizeMenu.setItems(FXCollections.observableArrayList(0.5, 1.5));
    }

    @FXML
    private void enterMoney(ActionEvent event) {
        textDisplay.setText(d.addMoney(moneySlider.getValue()));
        moneySlider.setValue(0);
        sliderAmount.setText(String.format("%.2f", moneySlider.getValue()));
        
    }

    @FXML
    private void buy(ActionEvent event) {
        if (brandMenu.getValue() == null || sizeMenu.getValue() == null) {
            textDisplay.setText("Valitse ensin merkki ja koko!");
        } 
        else {
        textDisplay.setText(d.buyBottle(brandMenu.getValue(), sizeMenu.getValue()));
        }
    }

    @FXML
    private void moneyBack(ActionEvent event) {
        textDisplay.setText(d.returnMoney());
    }

    @FXML
    private void setSliderAmount(MouseEvent event) {
        sliderAmount.setText(String.format("%.2f", moneySlider.getValue()));
    }


    @FXML
    private void receipt(ActionEvent event) {
        try {
            textDisplay.setText(d.saveReceipt());
        } catch (IOException ex) {}
    }
}
