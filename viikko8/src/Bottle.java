

/**
 * Viikko8 tehtävä kaikki
 * @author Antti Vilkman 0521281
 * Olio-Ohjelmoinnin perusteet
 * 29.5.2018
 * Bottle.java
 */

public class Bottle {
    private String brand;
    private String name;
    private double size;
    private double price;
    
    public Bottle() {
        // Default bottle
        brand = "Pepsi";
        name = "Pepsi Max";
        size = 0.5;
        price = 1.80;
    }
    
    public Bottle(String b, String n, double sz, double prc) {
        // Custom bottle
        brand = b;
        name = n;
        size = sz;
        price = prc;
    }
    
    public String getName() {
        return name;
    }
    
    public double getPrice() {
        return price;
    }
    
    public double getSize() {
        return size;
    }
}
