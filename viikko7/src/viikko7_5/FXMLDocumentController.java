/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko7_5;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;

/**
 *
 * @author Ana
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Label label;
    @FXML
    private TextField inputField;
    @FXML
    private Button saveButton;
    @FXML
    private Button loadButton;
    private TextInputDialog dialog;
    
    @FXML
    private void loadButtonAction(ActionEvent event) {
        String fileName;
        dialog = new TextInputDialog();
        dialog.setHeaderText("Anna tiedoston nimi: ");
        Optional<String> input = dialog.showAndWait();
        readFile(input.get()); 
    }
    
    @FXML
    private void saveButtonAction(ActionEvent event) {
        String fileName;
        dialog = new TextInputDialog();
        dialog.setHeaderText("Anna tiedoston nimi: ");
        Optional<String> input = dialog.showAndWait();
        writeFile(inputField.getText(), input.get()); 
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    public void readFile(String inputFileName) {
        try {
            BufferedReader in;
            
            in = new BufferedReader(new FileReader(inputFileName));
            String inputLine;
            inputField.clear();
            while((inputLine = in.readLine()) != null) {
                inputField.setText(inputLine);
            }
            in.close();       
        } catch (IOException ex) {
            System.out.println("File not found!");
        }
    }
    
    public void writeFile(String output, String outputFileName) {
        try {
            BufferedWriter out;

            out = new BufferedWriter(new FileWriter(outputFileName));
            String inputLine;
            out.write(output);
            out.close();          
        } catch (IOException ex) {
            System.out.println("File not found!");
        }
    }
}
