/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.net.URL;
import java.util.ResourceBundle;
import javafx.concurrent.Worker;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.web.WebView;

/**
 *
 * @author Ana
 */

public class WebViewController implements Initializable {
    
    @FXML
    private Label label;
    @FXML
    private TextField urlField;
    @FXML
    private WebView web;
    private String currentURL;
    @FXML
    private Button shoutButton;
    @FXML
    private Button huudaButton;
    @FXML
    private Button previousButton;
    @FXML
    private Button nextButton;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        web.getEngine().getLoadWorker().stateProperty().addListener((observable, oldValue, newValue) -> {
            if (Worker.State.SUCCEEDED.equals(newValue)) {
                urlField.setText(web.getEngine().getLocation());
            }
        }); 
    }    

    @FXML
    private void loadAddress(ActionEvent event) {
        try {
            if (urlField.getText().equals("index.html")) {
                web.getEngine().load(getClass().getResource("index2.html").toExternalForm());
            } 
            else {
                web.getEngine().load("http://" + urlField.getText());
                //refreshURL();
            }
        } catch (IllegalArgumentException | IndexOutOfBoundsException ex) {}
    }

    @FXML
    private void refreshPage(ActionEvent event) {
        try {
            //refreshURL();
            web.getEngine().load(web.getEngine().getLocation());
        } catch (IllegalArgumentException ex) {}
    }
    
    @FXML
    private void shout(ActionEvent event) {
        web.getEngine().executeScript("document.shoutOut()");
    }

    @FXML
    private void shoutRevert(ActionEvent event) {
        web.getEngine().executeScript("initialize()");
    }

    @FXML
    private void goPrevious(ActionEvent event) {
        try {
        web.getEngine().getHistory().go(-1);
        //refreshURL();
        } catch (IndexOutOfBoundsException e) {}
    }

    @FXML
    private void goNext(ActionEvent event) {
        try {
        web.getEngine().getHistory().go(1);
        //refreshURL();
        } catch (IndexOutOfBoundsException e) {}
    }
    
    @FXML
    private void refreshURL() {
        urlField.setText(web.getEngine().getLocation());
    } 
}
